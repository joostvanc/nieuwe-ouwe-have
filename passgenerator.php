<?php
$password = "test123";
$salt = strtr(base64_encode(openssl_random_pseudo_bytes(18)), '+', '.');
$new = crypt($password, sprintf('$2y$%02d$%s', 13, $salt));

echo $new;
?>