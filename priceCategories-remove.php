<?php
require_once 'includes/globals.php';
require_once 'includes/requireSession.php';
require_once 'includes/functions.php';
require_once 'includes/connectdb.php';

$pageid = 17;

if (hasacces($pageid) == true) {
?>
<!DOCTYPE html>
<html lang="nl">

<head>
    <?php

    include_once 'includes/head.php';

    ?>

    <title><?php echo SITE_TITLE; ?> - Prijs Categorieeen</title>
</head>

<body>

<?php include_once 'includes/wrapper.php'; ?>

<!-- Sidebar -->
<?php

include_once 'includes/sidebar.php';

?>
<!-- /#sidebar-wrapper -->

<!-- Page Content -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>Prijs Categorieeen <small>Voeg toe</small></h1>
                </div>
                <p>Op deze pagina kunt u gegevens in het kasboek zetten, deze worden direct opgeslagen wanneer u op volgende drukt</p>
                <p>Wanneer u meerdere kasboek gegevens wilt invoeren, kunt u kiezen voor "nog 1 toevoegen, wanneer u klaar bent kunt u weer op volgende drukken om verder te gaan</p>
                
                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="invoices.php">Facturen</a></li>
                        <li role="presentation"  ><a href="invoices-add.php">Enkele factuur toevoegen</a></li>
                        <li role="presentation"  ><a href="invoiceall-add.php">Massa factuur versturen</a></li>
                        <li role="presentation"><a href="priceCategories-add.php">Prijs Categorieen toevoegen (enkele facturen)</a>
                        <li role="presentation"><a href="priceCategoriesall-add.php">Prijs Categorieen toevoegen (massa facturen)</a>                        <li role="presentation" class="active"><a href="priceCategories-remove.php">Prijs Categorieen verwijderen</a>
                    </ul>


                  <?php
                if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['member'])) {

                    $priceCat = cleanInput($_POST['member']);

                    if (validateNumber($priceCat, 1, 11)) {

                        $dataManager->where('ID', $priceCat);
                        $remove = $dataManager->delete('oh_price_category');

                        if ($remove) {
                            echo '<div class="alert alert-success" role="alert">de categorie is succesvol verwijderd!</div>';
                        } else {
                            echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof er een fout is met de verbinding van de database...</div>';
                            echo '<p>Klik <a href="members-remove.php">hier</a> om het opnieuw te proberen.</p>';
                        }

                    } else {
                        echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof niet alle gegevens zijn ingevuld...</div>';
                        echo '<p>Klik <a href="members-remove.php">hier</a> om het opnieuw te proberen.</p>';
                    }

                } else {
                    ?>
                    <hr />
                    <form role="form2" method="POST" enctype="multipart/form-data" name="catDelete" name="catDel" id="deletePriceCategory">
                        <div class="form-group">
                            <label for="ID">Kies een categorie om te verwijderen:</label>
                            <select class="form-control" name="member" id="member">
                                <option value="" selected disabled></option>
                                <?php

                                $priceCats = $dataManager->get('oh_price_category');

                                foreach($priceCats as $priceCat) {
                                    $eigenaar = generateName($priceCat['Naam']);

                                    echo '<option value="' . $priceCat["ID"] . '">' . $eigenaar . '</option>';
                                }

                                ?>
                            </select>
                        </div>
                        <input name="catdelete" type="submit" class="btn btn-primary" onclick="document.forms['catDel'].submit()" value="Verwijder" action>
                        
                    </form>
                <?php
                }
                ?>

                <hr/>
                
                </div>
            </div>
        </div>
    </div>

<!-- /#page-content-wrapper -->


<!-- /#wrapper -->

<!-- Footer -->
<?php

include_once 'includes/footer.php';

?>
<?php 

} else {
	
	header("Location: index.php");	
}
?>
</body>

</html>