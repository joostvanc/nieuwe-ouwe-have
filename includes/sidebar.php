<div id="sidebar-wrapper"> <a class="btn btn-primary" id="menu-toggle"> <i class="fa fa-lg fa-angle-left"></i> </a>
  <ul class="sidebar-nav">
    <li class="sidebar-brand"> <a href="index.php"> De 'n Ouwe Haven </a> </li>
    
    <?php if(isset($_SESSION['id'])) { ?>
    <li> <a href="index.php">Home</a> </li>
    <li> <a href="ships.php"><i class="fa fa-ship"></i> Schepen</a> </li>
    <li> <a href="settings.php"><i class="fa fa-cog"></i> Instellingen</a> </li>
    
    <?php
		$userquery = $dataManager->rawQuery("SELECT * FROM oh_tasks, oh_permissions WHERE oh_permissions.userid = '".$_SESSION['id']."' AND oh_tasks.taskid = oh_permissions.taskid AND oh_tasks.menu = 1");
	foreach($userquery as $userid) {
	
	echo ('<li> <a href="'.$userid['page'].'"><i class="fa '.$userid['icon'].'"></i> '.$userid['taskname'].'</a> </li>');
	
	}
	?>
    <li> <a href="logout.php"><i class="fa fa-sign-out"></i> Log Out</a> </li>
    
    <?php
} else {
?>  
    <li> <a href="login.php"><i class="fa fa-sign-in"></i> Log in</a> </li>
    
<?php } ?>
      </ul>
</div>
