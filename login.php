<!DOCTYPE html>
<html lang="nl">

<head>
    <?php

        include_once 'includes/head.php';

    ?>

<?php
    require_once 'includes/globals.php';
	require_once 'includes/functions.php';
    require_once 'includes/connectdb.php';
	

?>
    <title><?php echo SITE_TITLE; ?> - Webportal</title>

</head>

<body>

    <?php include_once 'includes/wrapper.php'; ?>

        <!-- Sidebar -->
        <?php

            include_once 'includes/sidebar.php';

        ?>
        <!-- /#sidebar-wrapper -->
 
 <div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                       Inloggen
                    </h1>
                </div>       

<?php

if(!isset($_SESSION['id'])) {

	if(isset($_POST['login']) AND !empty($_POST['username']) AND !empty($_POST['password'])) {
		
		
		$username = $dataManager->escape($_POST['username']);
		$input_password = $dataManager->escape($_POST['password']); // the submitted password

		$dataManager->where('user_login = "'.$username.'"');
		$data = $dataManager->getOne('oh_members');

		$db_password = $data['user_pass']; // field with the password hash

		$given_hash = crypt($input_password, $db_password);
		
if (isEqual($given_hash, $db_password)) {
	
	//$_SESSION['ip'] = $_SERVER['REMOTE_ADDR'];
	$_SESSION['username'] =  $username;
	$_SESSION['id'] = $data['ID'];
	$_SESSION['level'] = $data['level'];
	$_SESSION['password'] = $db_password;
	
			// Store member details in session
		
			$dataManager->where('ID', $data['ID']);
			$user = $dataManager->get('oh_members', 1);

			// Store details in session
			$_SESSION['member_details'] = $user[0];
	
		?>
        <meta HTTP-EQUIV="REFRESH" content="0; url=index.php">
       		<?php
		exit(); 
} 

else { 
echo '<p>U heeft foute gegevens ingevuld. Probeer het <a href="'.$_SERVER["PHP_SELF"].'">opnieuw!</a></p>';
} 
	
	} else {
?>
<form name="forms" method="post">
<table class='table table-bordered table-hover'>
<tr>
<td><label for="username">Gebruikersnaam</label></td>
<td><input type="text" value="<?php if(isset($_POST['login'])) { echo $_POST['username']; } ?>" name="username" <?php if(isset($_POST['login']) AND $_POST['username'] == ""){ echo 'style="border: #c85041 solid 2px;"'; } ?> />
</td>
</tr>
<tr>
<td><label for="username">Wachtwoord</label></td>
<td><input type="password" name="password" <?php if(isset($_POST['login']) AND $_POST['password'] == ""){ echo 'style="border: #c85041 solid 2px;"'; } ?> /></td>
</tr>
</table>			

<input class='btn btn-default' type="submit" name="login" value="Login" />

<p><a href="passforget.php"><br>
  Wachtwoord vergeten</a></p>
</form>
<?php
	} 
} else {
	
	echo ("U bent al ingelogd!");
	
}
?>

                <hr/>

            </div>
        </div>
    </div>
</div>
  
    <?php

        include_once 'includes/footer.php';

    ?>