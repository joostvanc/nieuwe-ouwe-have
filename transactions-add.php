<?php
    require_once 'includes/globals.php';
    require_once 'includes/requireSession.php';
    require_once 'includes/functions.php';
    require_once 'includes/connectdb.php';
?>
<!DOCTYPE html>
<html lang="nl">
	<head>
	    <?php include_once 'includes/head.php'; ?>
	    <title><?php echo SITE_TITLE; ?> - Transacties - Toevoegen</title>
	</head>
	<body>
		<?php include_once 'includes/wrapper.php'; ?>
	    <!-- Sidebar -->
	    <?php include_once 'includes/sidebar.php'; ?>
	    <!-- /#sidebar-wrapper -->
	    <!-- Page Content -->
	    <div id="page-content-wrapper">
	        <div class="container-fluid">
	            <div class="row">
	                <div class="col-lg-12">
	                    <div class="page-header">
	                        <h1>Transacties <small>Toevoegen</small></h1>
	                    </div>
	                    <p>Voeg nieuwe transacties toe.</p>
	                    <ul class="nav nav-tabs">
	                        <li role="presentation"><a href="transactions.php">Credit</a></li>
	                        <li role="presentation"><a href="transactions-debet.php">Debet</a></li>
	                        <li role="presentation" class="active"><a href="transactions-add.php">Voeg transacties toe</a></li>
	                    </ul>
						<?php
							if ($_SERVER['REQUEST_METHOD'] == 'POST') {
								
								
								$cat_id = cleanInput($_POST['cat_id']);
								$sender = cleanInput($_POST['sender']);
								$valuta = cleanInput($_POST['valuta']);	
								$rentdate = date('Y-m-d');
								$code = cleanInput($_POST['code']);	
								$amount = cleanInput($_POST['amount']);
								$receiver = cleanInput($_POST['receiver']);
								$namereceiver = cleanInput($_POST['namereceiver']);
								$bookdate = date('Y-m-d');
								$bookcode = cleanInput($_POST['bookcode']);
							    $desc = cleanInput($_POST['desc']);
							    
							    
							    if( validateInput($cat_id, 1, 64) &&
							    	validateInput($sender, 2, 50) &&
							    	validateInput($valuta, 2, 3) &&
							    	validateDate($rentdate, 'Y-m-d') &&
							    	validateInput($code, 0, 1) &&
							        validateInput($amount, 1, 32) &&
							        validateInput($receiver, 2, 50) &&
							        validateDate($bookdate, 'Y-m-d') &&
							        validateInput($bookcode, 2, 50) &&
							        validateInput($desc, 2, 64))
							    {
							        $data = array(
							        	'Categorie_ID' => $cat_id,
							        	'Rekeningnummer' => $sender,
							        	'Muntsoort' => $valuta,
							        	'Rentedatum' => $rentdate,
							        	'Code' => $code,
							        	'Bedrag' => $amount,
							        	'Tegenrekening' => $receiver,
							        	'Naam_Ontvanger' => $namereceiver,
							        	'Boekdatum' => $bookdate,
							        	'Boekcode' => $bookcode,
							            'Omschrijving' => $desc,
							            );
									
							        $insert = $dataManager->insert('oh_transactions', $data);
									if($insert) {
									    echo '<div class="alert alert-success" role="alert">Bedankt voor het toevoegen van de gegevens, ze zijn succesvol verwerkt!</div>';
									    echo '<p>Klik <a href="/">hier</a> om naar de hoofdpagina te gaan.</p>';
									    echo "<p>Of klik <a href=".$_SERVER['REQUEST_URI'].">hier</a> om nog een transactie toe te voegen.";
									} else {
									    echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof er een fout is met de verbinding van de database...</div>';
									    echo "<p>Klik <a href=".$_SERVER['REQUEST_URI'].">hier</a> om het opnieuw te proberen.</p>";
									}
							
							    } else {
							        echo '<div class="alert alert-danger" role="alert">Het lijkt er op alsof niet alle gegevens zijn ingevuld...</div>';
							        echo "<p>Klik <a href=".$_SERVER['REQUEST_URI'].">hier</a> om het opnieuw te proberen.</p>";
							    }         
							} 
						?>
						<form id="addTransactionForm" role="form" method="POST" enctype="multipart/form-data">
	                        <div class="form-group">  
	                        	<div class="col-md-14" align="left">  
	                        		<label for="cat_id">Kies een rubriek:</label>	
		                        		<select class="form-control" name="cat_id" id="cat_id">
				                        	<option value="" selected disabled></option>
				                            <?php
				                                $categories = $dataManager->get('oh_categories');
				                                foreach ($categories as $rubriek) {
				                                	$categorie = generateName($rubriek['Naam']);
													
				                                    echo '<option value="' . $rubriek['ID'] . '">' . $categorie . '</option>';
				                                }
				                            ?>
				                        </select>
				                        <label for="sender">Rekeningnummer afzender</label>
			                            <input type="text" class="form-control" name="sender">
			                            
			                            <label for="valuta">Valuta</label>
			                            <input type="text" class="form-control" name="valuta">
			                        
			                            <label for="rentdate">Rentedatum:</label>
			                            <input type="text" value="<?php echo date("d/m/Y") ?>" class="form-control formDate" name="rentdate" id="rentdate">
	                            
	                            
					                        <div align="left"  class="form-group col-md-10">
					                        	<label for="amount">Bedrag:</label>
					                        	<input type="number" class="form-control" name="amount" id="amount" number required data-progression="" data-helper="Vul hier uw bedrag in.">
					                        </div>
					                            <div class="form-group col-md-2">
					                                <label for="code">Debet/Credit:</label>
					                                <select class="form-control" name="code" id="code">
					                                    <option value="D">Debet</option>
					                                    <option value="C">Credit</option>                               
					                                </select>
					                            </div>
					                    	
					                    
			                            
			                            
			                            	<div align="left"  class="form-group col-md-10">
					                            <label for="receiver">Rekeningnummer Ontvanger:</label>
					                            <input type="text" class="form-control" name="receiver">
					                        </div>
				                            <div class="form-group col-md-2">
					                            <label for="namereceiver">Naam Ontvanger:</label>
					                            <input type="text" class="form-control" name="namereceiver">
				                            </div>
				                        </div>
				                            
			                            <label for="bookdate">Boek datum:</label>
			                            <input type="text" value="<?php echo date("d/m/Y") ?>" class="form-control formDate" name="bookdate" id="bookdate">
			                            
			                            <label for="bookcode">Kies een Betaalmethode:</label>
				                        <select class="form-control" name="bookcode" id="bookcode">
				                        	<option value="" selected disabled></option>
				                            <?php
				                                $bookcodes = $dataManager->get('oh_bookcodes');
				                                foreach ($bookcodes as $betaalmethode) {
				                               $code = generateName($betaalmethode['betaalmethode']);
													
				                                    echo '<option value="' . $betaalmethode['id'] . '">' . $code . '</option>';
				                                }
				                            ?>
				                        </select>
			                         
			                            <label for="desc">Omschrijving:</label>
			                            <input type="text" class="form-control" name="desc">
			                         
			                            
	                        </div>    
	                        </div>
	                        <button type="submit" class="btn btn-primary">Toevoegen</button>
	                    </form>
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- /#page-content-wrapper -->
	    <!-- /#wrapper -->
	
	    <!-- Footer -->
	    <?php include_once 'includes/footer.php'; ?>
	</body>
</html>