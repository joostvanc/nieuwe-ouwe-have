<html lang="nl">

<head>
    <?php

        include_once 'includes/head.php';

    ?>

<?php
    require_once 'includes/globals.php';
	require_once 'includes/functions.php';
    require_once 'includes/connectdb.php';
	

?>
    <title><?php echo SITE_TITLE; ?> - Webportal</title>

</head>

<body>

    <?php include_once 'includes/wrapper.php'; ?>

        <!-- Sidebar -->
        <?php

            include_once 'includes/sidebar.php';

        ?>
        <!-- /#sidebar-wrapper -->
<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1>
                        Wachtwoord vergeten
                    </h1>
                </div>
<?php if(!isset($_POST['aanvragen'])) { ?> 
<p>Vul hieronder uw e-mail adres in:
<form method="post">
		<b>E-mail adres:</b>
				<br>
				<input class="form-control" type="text" value="" name="email">
				<br>
				<input class='btn btn-default' type="submit" name="aanvragen" value="Wachtwoord aanvragen">
</form>
<?php
}
if(isset($_POST['aanvragen'])) {
	
echo ("Succes! Als uw e-mail adres overeen komt met een gebruiker in onze database, dan zul u een e-mail ontvangen met uw nieuwe wachtwoord.<br>");
$result = $mysqli->query("SELECT email FROM oh_members WHERE email = '".cleaninput($_POST['email'])."'");
$num_rows = mysqli_num_rows($result);

if ($num_rows > 0) {

function random_password( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}
$password = random_password(8);
			$salt = strtr(base64_encode(openssl_random_pseudo_bytes(18)), '+', '.');
			$new = crypt($password, sprintf('$2y$%02d$%s', 13, $salt));
    $mysqli->query("UPDATE user SET password='" . $mysqli->real_escape_string($new) . "' WHERE email='" . $mysqli->real_escape_string($_POST['email']) . "'");
  
$validemail = filter_input(INPUT_POST, ''.$_POST['email'].'', FILTER_VALIDATE_EMAIL);
			if ($validemail) {
  // Reset Mail
			$message = '<html><body>';
			$message .= '<p><strong>Beste, </strong></p>';
			$message .= '<p>Iemand heeft een nieuw wachtwoord aangevraagd voor uw account.3
			</p>';
			$message .= "<br /><strong>Uw nieuwe wachtwoord is:</strong>".$password."";
			$message .= "<p><br /Met vriendelijke groet,</p>";
			$message .= "<p><strong>:: Webmater Ouwe Haven<br />";
             
			
			 
			$to = '';
			
			$subject = 'Nieuwe wachtwoord voor uw website';
			
			$headers = "From: noreply@dnouwehaven.nl.nl\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

          	mail($to, $subject, $message, $headers);
}  
}
}
?>
                <hr/>

            </div>
        </div>
    </div>
</div>

    <?php

        include_once 'includes/footer.php';

    ?>